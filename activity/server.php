<?php
session_start();

// Check if the user clicked the login button
if (isset($_POST['login'])) {
    $username = $_POST['email'];
    $password = $_POST['password'];

    // Check if the credentials are correct
    if ($username === 'johnsmith@gmail.com' && $password === '1234') {
        $_SESSION['email'] = $username;
    } else {
        // If the credentials are incorrect, redirect back to the login page
        header('Location: ./index.php');
        exit();
    }
}

// Check if the user clicked the logout button
if (isset($_POST['logout'])) {
    // Clear the user information and redirect back to the login page
    session_unset();
    session_destroy();
    header('Location: ./index.php');
    exit();
}

// If the user is logged in, redirect to the index page
if (isset($_SESSION['email'])) {
    header('Location: ./index.php');
    exit();
}

?>
